import React from "react";
import "./searchPanel.css";
import { Form } from "react-bootstrap";

export default function SearchPanel() {
  return (
    <div className="d-inline-block search-panel">
      <Form.Control placeholder="type to search" />
    </div>
  );
}
